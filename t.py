import pygame, string
#from colour import Color
import recursos
from configparser  import SafeConfigParser
from datetime import date
from pygame.locals import *
from pygame import *

class boton(object):
	def __init__(self, img1,img2,h,l):
		self.imgn=img1
		self.imgs=img2
		self.imga=self.imgn
		self.rect=self.imga.get_rect()
		self.rect.left,self.rect.top=(h,l)
	def update(self,screen,cursor,h,l):
		self.rect.left,self.rect.top=(h,l)
		if cursor.colliderect(self.rect):
			#print(self.rect)
			self.imga=self.imgs
		else: self.imga=self.imgn
		screen.blit(self.imga,self.rect)
class cursor(pygame.Rect):
	def __init__(self): 
		pygame.Rect.__init__(self,0,0,1,1)
	def update(self):
		self.left,self.top=pygame.mouse.get_pos()
class text(object):
	def __init__(self,FontSize, FontName): 
		pygame.font.init()
		self.font=pygame.font.Font(FontName,FontSize)
		self.size=FontSize
	def render(self,surface,text,color,pos):
		#text=unicode(text,"UTF-8")
		x,y=pos
		for i in text.split("\r"):
			text=surface.blit(self.font.render(i,1,color),(x,y))
			y += self.size
		return text
class interface(object):
	def __init__(self):
		fuente_general=recursos.fuente1
		self.pantalla=pygame.display.set_mode((1300,700),RESIZABLE)
		self.ventana="Finanzas"
		self.rect_nombre=[0,0,0,0]
		self.texto={"sxcht":text(10,fuente_general),"xcht":text(15,fuente_general),"cht":text(20,fuente_general),"mt":text(30,fuente_general),"lt":text(40,fuente_general)}
		self.espacio_texto={"sxcht":(10,13),"xcht":(13,18),"cht":(17,24),"mt":(24,36),"lt":(32,48)}

		self.nombre_botones_menu_principal=["Finanzas","Diario","Logros","Configuracion","Usuario"] #,"Acerca","Cuenta"
		self.botones_menu_principal={"Finanzas":[boton(recursos.finanzas,recursos.finanzas,10,60),"FINANZAS",10,60],"Diario":[boton(recursos.diario,recursos.diario,10,102),"DIARIO",10,102],"Logros":[boton(recursos.logros,recursos.logros,10,144),"LOGROS",10,144],"Configuracion":[boton(recursos.configuracion,recursos.configuracion,10,186),"CONFIGURACION",10,186],"Usuario":[boton(recursos.mando,recursos.mando,10,8),"USUARIO",10,8]}
		self.nombre_botones_menu_principal_2=["Balance","Entradas","Logros","Nivel","Animo"] #,"Acerca","Cuenta"
		self.botones_menu_principal_2={"Balance":[boton(recursos.finanzas,recursos.finanzas,0,0),"BALANCE",0,0],"Entradas":[boton(recursos.diario,recursos.diario,0,0),"ENTRADAS",0,0],"Logros":[boton(recursos.logros,recursos.logros,0,0),"LOGROS",0,0],"Nivel":[boton(recursos.nivel,recursos.nivel,0,0),"NIVEL",0,0],"Animo":[boton(recursos.smile,recursos.smile,0,0),"ANIMO",0,0]}

		self.botones_sem={"UPS":[boton(recursos.up_sem,recursos.up_sem,0,0),"UPS",0,0],"DWS":[boton(recursos.dw_sem,recursos.dw_sem,0,0),"DWS",0,0]}
		#self.botones_finanzas={"Agregar transaccion":[0,boton(recursos.agregar,recursos.agregar_,1150,80),"Agregar transaccion",1150,80],"Quitar transaccion":[0,boton(recursos.quitar,recursos.quitar_,1190,80),"Quitar transaccion",1190,80]}
		self.botones_finanzas={"Agregar transaccion":[0,boton(recursos.addd,recursos.addd,1150,80),"Agregar transaccion",1100,60],"Quitar transaccion":[0,boton(recursos.adddd,recursos.adddd,1190,80),"Quitar transaccion",1190,60]}
		self.nombre_botones_finanzas=["Agregar transaccion","Quitar transaccion"]

		self.tipo_transaccion_eleccion,self.gasto_eleccion,self.deuda_eleccion=0,0,0
		self.semana_eleccion=date.today().isocalendar()[1]
		self.comentario_transaccion_mensaje="Escribe aqui un comentario"
		self.ingreso_acumulado,self.gasto_acumulado,self.cuentas,self.transacciones,self.dinero_monto=0,0,{},{},0
		self.clases_gastos=["Automovil","Casa","Comida","Entretenimiento","Mascotas","Personal","Regalos","Salud","Servicios","Viajes"]
		self.clases_gasto={"Automovil":0,"Entretenimiento":0,"Comida":0,"Regalos":0,"Salud":0,"Casa":0,"Personal":0,"Mascotas":0,"Servicios":0,"Viajes":0,}
		self.tipo_transaccion=["Aporte a deuda","Gasto corriente","Ingreso corriente"]
		self.nombre_propio,self.balance,self.numero_logros,self.numero_entradas_diario,self.nivel,self.deuda_total,self.fondo_mundial,self.monto_transaccion="",0,0,0,0,[0,[0,0,0,0]],0,0
		self.cargar_configuraciones()
		self.a_nombre=(self.rect_nombre[2]+60)
		self.espacio_entre_indicadores,self.a_balance,self.a_entradas,self.a_logros,self.a_nivel=0,400,100,100,100
		self.menu_principal()
		self.carga_dependencias()

	def carga_dependencias(self):
		self.a_nombre=(self.rect_nombre[2]+60)
		a_animo=52
		self.espacio_entre_indicadores=(self.pantalla.get_width()-self.a_nombre-self.a_balance-self.a_entradas-self.a_logros-self.a_nivel-a_animo)/5
		self.botones_menu_principal_2={"Balance":[boton(recursos.finanzas,recursos.finanzas,0,0),"BALANCE",self.a_nombre+self.espacio_entre_indicadores,8],"Entradas":[boton(recursos.diario,recursos.diario,0,0),"ENTRADAS",self.a_nombre+self.a_balance+(2*self.espacio_entre_indicadores),8],"Logros":[boton(recursos.logros,recursos.logros,0,8),"LOGROS",self.a_nombre+self.a_balance+self.a_entradas+(3*self.espacio_entre_indicadores),8],"Nivel":[boton(recursos.nivel,recursos.nivel,0,8),"NIVEL",self.a_nombre+self.a_balance+self.a_entradas+self.a_logros+(4*self.espacio_entre_indicadores),8],"Animo":[boton(recursos.smile,recursos.smile,0,0),"ANIMO",self.a_nombre+self.a_balance+self.a_entradas+self.a_logros+self.a_nivel+(5*self.espacio_entre_indicadores),8]}
	def cargar_configuraciones(self):
		parser = SafeConfigParser()
		parser.read('./Datos/config.cfg')
		if parser.getint('general','color_')==0:
			self.color_principal=(30,30,30)
			self.color_secundario=(50,50,50)
			self.color_caja=(80,80,80)
			self.color_texto=(250,250,250)
		else:
			self.color_principal=(250,250,250)
			self.color_texto=(50,50,50)
		self.color_personalizado=Color(parser.get('general','color__'))
		self.imagenes_registradas=parser.getint('general','capturas')
		self.nombre_propio=parser.get('general','nombre')

		parser = SafeConfigParser()
		parser.read('./Datos/cuentas.cfg')
		self.numero_cuentas=len(parser.sections())
		for section in parser.sections():
			cadena=[]
			for valor,no in parser.items(section):
				cadena.append(int(no))
			self.cuentas[section]=cadena

		parser = SafeConfigParser()
		parser.read('./Datos/transacciones.cfg')
		self.numero_transacciones=len(parser.sections())
		for section in parser.sections():
			cadena=[]
			for valor,no in parser.items(section):
				try:
					cadena.append(int(no))
				except:
					cadena.append(no)
			self.transacciones[int(section)]=cadena
		for transaccion in self.transacciones:
			anterior=0
			if self.transacciones[transaccion][0]=="D":
				self.cuentas[self.transacciones[transaccion][2]][0]+=self.transacciones[transaccion][4]
				self.cuentas[self.transacciones[transaccion][3]][0]+=self.transacciones[transaccion][4]
				self.gasto_acumulado+=self.transacciones[transaccion][4]
			if self.transacciones[transaccion][0]=="G":
				self.cuentas[self.transacciones[transaccion][2]][0]+=self.transacciones[transaccion][4]
				self.clases_gasto[self.transacciones[transaccion][3]]+=self.transacciones[transaccion][4]
				self.gasto_acumulado+=self.transacciones[transaccion][4]
			if self.transacciones[transaccion][0]=="S":
				self.cuentas[self.transacciones[transaccion][2]][0]+=self.transacciones[transaccion][4]
				self.ingreso_acumulado+=self.transacciones[transaccion][4]

		for cuenta in self.cuentas:
			self.cuentas[cuenta][2]=[0,0,0,0]
			if self.cuentas[cuenta][1]==1:
				self.balance+=self.cuentas[cuenta][0]
			if self.cuentas[cuenta][0]<0:
				self.deuda_total[0]+=self.cuentas[cuenta][0]

		self.nombres_cuentas,self.nombres_gastos,self.nombres_deudas=[],[],[]
		for key,value in self.cuentas.items():
			if value[0]!=0:
				if key=="Balance":
					self.nombres_cuentas.append(key)
				else:
					self.nombres_deudas.append(key)
		for key,value in self.clases_gasto.items():
			self.nombres_gastos.append(key)


		#print(self.cuentas)
		#print(self.nombres_cuentas)
		#print(self.nombres_deudas)
	def pantalla_cargando(self):
		self.pantalla.fill(self.color_principal)
		self.caja("lt","Cargando",[55+self.separacion+20,70],7)
	def AAfilledRoundedRect(self,surface,rect,color,radius=0.4):
		rect         = Rect(rect)
		color        = Color(*color)
		alpha        = color.a
		color.a      = 0
		pos          = rect.topleft
		rect.topleft = 0,0
		rectangle    = Surface(rect.size,SRCALPHA)
		circle       = Surface([min(rect.size)*3]*2,SRCALPHA)
		draw.ellipse(circle,(0,0,0),circle.get_rect(),0)
		circle       = transform.smoothscale(circle,[int(min(rect.size)*radius)]*2)
		radius              = rectangle.blit(circle,(0,0))
		radius.bottomright  = rect.bottomright
		rectangle.blit(circle,radius)
		radius.topright     = rect.topright
		rectangle.blit(circle,radius)
		radius.bottomleft   = rect.bottomleft
		rectangle.blit(circle,radius)
		rectangle.fill((0,0,0),rect.inflate(-radius.w,0))
		rectangle.fill((0,0,0),rect.inflate(0,-radius.h))
		rectangle.fill(color,special_flags=BLEND_RGBA_MAX)
		rectangle.fill((255,255,255,alpha),special_flags=BLEND_RGBA_MIN)
		return surface.blit(rectangle,pos)

	def caja(self,tamano_letra,mensaje,inicio,numero_caracteres,color_rect=None,numero=False,moneda=False,color_txt=False):
		if color_txt==False:
			color_txt=self.color_texto
		rectangulo_caja=[inicio[0],inicio[1],numero_caracteres*self.espacio_texto[tamano_letra][0],self.espacio_texto[tamano_letra][1]]
		if color_rect!=None:
			boton=pygame.draw.rect(self.pantalla,self.color_caja,rectangulo_caja)
			pygame.draw.rect(self.pantalla,color_rect,rectangulo_caja,1)
		if numero==True:
			boton1=self.texto[tamano_letra].render(self.pantalla,str(mensaje),color_txt,(rectangulo_caja[0]+2,rectangulo_caja[1]))
		elif moneda==True:
			if mensaje>0:
					boton1=self.texto[tamano_letra].render(self.pantalla,self.monetizar_cifra(mensaje),Color("green"),(rectangulo_caja[0]+2,rectangulo_caja[1]))
			if mensaje<=0:
					boton1=self.texto[tamano_letra].render(self.pantalla,self.monetizar_cifra(mensaje),Color("red"),(rectangulo_caja[0]+2,rectangulo_caja[1]))
		else:
			boton1=self.texto[tamano_letra].render(self.pantalla,str(mensaje),color_txt,(rectangulo_caja[0]+2,rectangulo_caja[1]))
		if color_rect!=None:
			return boton
		else:
			return boton1

	def actualizar_caja(self,tl,string,rc,nc): 
		#fontobject = pygame.font.Font(self.fuente,20)
		#pygame.draw.rect(screen,(interface.color_principal[0]-20,interface.color_principal[1]-20,interface.color_principal[2]-20),rectangulo_caja)
		#pygame.draw.rect(screen,recursos.RED,rectangulo_caja,1)
		#chtext.render(screen,message,recursos.RED,(rectangulo_caja[0],rectangulo_caja[1]))
		print("actualizar caja: ",rc)
		pygame.draw.rect(self.pantalla,Color('red'),rc)
		rc=self.caja(tl,string,rc,nc)
		pygame.display.flip()
		return rc

	def escribir_caja(self,mensaje,rectangulo_caja,numero_caracteres,tipo,tamano_letra):
		numero_caracteres-=1
		print(type(mensaje))
		current_string = mensaje
		rectangulo_caja[2]=13*(numero_caracteres+1)
		pygame.font.init()
		while 1:
			event = pygame.event.poll()
			print("escribir caja: ",rc)
			if event.type == KEYDOWN:
				inkey = event.key
				if inkey == 13:
					break
				elif inkey == K_BACKSPACE:
					current_string = current_string[0:-1]
			if len(current_string)<=numero_caracteres:
				if event.type == KEYDOWN:
					inkey = event.key
					#sprint(inkey)
					if tipo=="numerico" or tipo=="alfanumerico":
						if (inkey>=48 and inkey<=57) or (inkey>=256 and inkey<=265):
							if inkey>200:
								current_string+=chr(inkey-208)
								#current_string.append(chr(inkey-208))
							else:
								current_string+=chr(inkey)
								#current_string.append(chr(inkey))
					if tipo=="alfabeto" or tipo=="alfanumerico":
						if (inkey>=97 and inkey<=122):
							current_string+=chr(inkey)
							#current_string.append(chr(inkey))
					if inkey==32:
						current_string+=" "
						#current_string.append(" ")
			
			#print(current_string)
			rectangulo_caja=self.actualizar_caja(tamano_letra,current_string,rectangulo_caja,numero_caracteres)
		print(current_string)
		try:
			if tipo=="numerico":
				return int(current_string)
			else:
				return current_string
		except:
			if tipo=="numerico":
				return 0
			else:
				return "error"	
		#str.join(current_string,"")
	def finance(self):
		self.separacion=20
		self.e_caja_pequena=self.AAfilledRoundedRect(self.pantalla,(55+self.separacion,120,(0.3)*(self.pantalla.get_width()-(55+(3*self.separacion))),self.pantalla.get_height()-(120+self.separacion)),self.color_caja,0.05)
		self.e_caja_grande=self.AAfilledRoundedRect(self.pantalla,(self.e_caja_pequena[0]+self.e_caja_pequena[2]+self.separacion,120,(0.7)*(self.pantalla.get_width()-(55+(3*self.separacion))),self.e_caja_pequena[3]),self.color_caja,0.05)
		self.e_caja_verde=self.AAfilledRoundedRect(self.pantalla,(self.e_caja_grande[0],self.pantalla.get_height()-(50+self.separacion),self.e_caja_grande[2]/2,50),Color("green"),0.05)
		self.e_caja_roja=self.AAfilledRoundedRect(self.pantalla,(self.e_caja_grande[0]+self.e_caja_grande[2]/2,self.pantalla.get_height()-(50+self.separacion),self.e_caja_grande[2]/2,50),Color("red"),0.05)
		self.mostrar_cuentas()
		if self.ventana=="Finanzas":
			self.mostrar_transacciones()
		else:
			self.agregar_transaccion_pantalla()

	def mostrar_cuentas(self):
		y_secciones=0
		self.caja("lt","Cuentas",[55+self.separacion+20,70],7,color_rect=None)
		
		self.caja("cht","Cuenta",[55+self.separacion+10,130],15,color_rect=None)
		self.caja("cht","Monto",[(55+self.separacion+self.e_caja_pequena[2]-70),130],15,color_rect=None)
		pygame.draw.rect(self.pantalla,recursos.BLACK,(self.e_caja_pequena[0],self.e_caja_pequena[1]+32,self.e_caja_pequena[2],2))

		self.caja("cht","Balance",[55+self.separacion+10,160],15,color_rect=None)
		self.cuentas["Balance"][2]=self.caja("cht",self.cuentas["Balance"][0],[(55+self.separacion+self.e_caja_pequena[2])-self.cuentas["Balance"][2][2]-5,160],20,color_rect=None,moneda=True)
		for cuenta in self.cuentas:
			if self.cuentas[cuenta][0]!=0 and cuenta!="Balance":
				self.caja("cht",cuenta,[55+self.separacion+10,190+(y_secciones*30)],15,color_rect=None)
				self.cuentas[cuenta][2]=self.caja("cht",self.cuentas[cuenta][0],[(55+self.separacion+self.e_caja_pequena[2])-self.cuentas[cuenta][2][2]-5,190+(y_secciones*30)],20,color_rect=None,moneda=True)
				y_secciones+=1


		pygame.draw.rect(self.pantalla,recursos.BLACK,(self.e_caja_pequena[0],self.pantalla.get_height()-(30+self.separacion),self.e_caja_pequena[2],2))
		self.caja("cht","Deuda total",[55+self.separacion+10,self.pantalla.get_height()-(25+self.separacion)],15,color_rect=None)
		#chtext.render(self.pantalla,"Deuda total",self.color_texto,[110,self.pantalla.get_height()-(120+self.separacion)])
		#print(self.deuda_total)
		self.deuda_total[1]=self.caja("cht",self.deuda_total[0],[(55+self.separacion+self.e_caja_pequena[2])-self.deuda_total[1][2]-5,self.pantalla.get_height()-(25+self.separacion)],20,color_rect=None,moneda=True)
		#chtext.render(self.pantalla,str(self.deuda_total),recursos.RED,[350,650])
	def mostrar_transacciones(self):
		y_secciones=0
		self.caja("lt","Transacciones",[self.e_caja_grande[0]+20,70],7)
		self.caja("cht","Sem",[self.e_caja_grande[0],130],15)
		self.caja("cht","Cuenta",[self.e_caja_grande[0]+self.e_caja_grande[2]*0.06,130],15)
		self.caja("cht","Comentario",[self.e_caja_grande[0]+self.e_caja_grande[2]*0.305,130],15)
		self.caja("cht","Monto",[self.e_caja_grande[0]+self.e_caja_grande[2]*0.55,130],15)
		for i in range(date.today().isocalendar()[1],0,-1):
			for transaccion in self.transacciones:
				if self.transacciones[transaccion][5]==i:
					pygame.draw.rect(self.pantalla,self.color_caja,(self.e_caja_grande[0],150+(y_secciones*40),self.e_caja_grande[2],40))
					self.caja("lt",self.transacciones[transaccion][5],[self.e_caja_grande[0]+5,150+(y_secciones*40)],7,color_rect=None)
					if self.transacciones[transaccion][0]=="S":
						self.caja("lt",self.transacciones[transaccion][2],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.06,150+(y_secciones*40)],15)
					else:
						self.caja("lt",self.transacciones[transaccion][3],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.06,150+(y_secciones*40)],15)
					self.caja("lt",self.transacciones[transaccion][1],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.305,150+(y_secciones*40)],15)
					self.caja("lt",self.transacciones[transaccion][4],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.55,150+(y_secciones*40)],15,moneda=True)
					y_secciones+=1
		pygame.draw.rect(self.pantalla,recursos.BLACK,(self.e_caja_grande[0],self.e_caja_grande[1]+32,self.e_caja_grande[2],2))

	def agregar_transaccion_pantalla(self):
		y_secciones=0
		self.caja("cht","Semana actual: "+str(date.today().isocalendar()[1]),[self.e_caja_grande[0],130],15)
		self.caja("lt","Agregar transaccion",[self.e_caja_grande[0]+20,70],7)
		self.eleccion_semana=self.caja("lt",self.semana_eleccion,[self.e_caja_grande[0]+5,150+(y_secciones*40)],7)
		self.eleccion_tipo_transaccion=self.caja("lt",self.tipo_transaccion[self.tipo_transaccion_eleccion],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.06,150+(y_secciones*40)],15)
		if self.tipo_transaccion_eleccion==0:
			self.eleccion_deuda=self.caja("lt",self.nombres_deudas[self.deuda_eleccion],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.55,150+(y_secciones*40)],15)
		elif self.tipo_transaccion_eleccion==1:
			self.eleccion_gasto=self.caja("lt",self.clases_gastos[self.gasto_eleccion],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.55,150+(y_secciones*40)],15)
		elif self.tipo_transaccion_eleccion==2:
			self.caja("lt","Balance",[self.e_caja_grande[0]+self.e_caja_grande[2]*0.55,150+(y_secciones*40)],15)

		y_secciones+=2
		self.comentario_transaccion=self.caja("lt",self.comentario_transaccion_mensaje,[self.e_caja_grande[0]+self.e_caja_grande[2]*0.06,150+(y_secciones*40)],7)
		#self.caja("lt",self.transacciones[transaccion][5],[self.e_caja_grande[0]+5,150+(y_secciones*40)],7,color_rect=None)
		#if self.transacciones[transaccion][0]=="S":
		#	self.caja("lt",self.transacciones[transaccion][2],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.06,150+(y_secciones*40)],15)
		#elif self.transacciones[transaccion][0]=="D":
		#	self.caja("lt",self.transacciones[transaccion][3],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.06,150+(y_secciones*40)],15)
		#elif self.transacciones[transaccion][0]=="G":
		#	self.caja("lt",self.transacciones[transaccion][3],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.06,150+(y_secciones*40)],15)
		#self.caja("lt",self.transacciones[transaccion][1],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.305,150+(y_secciones*40)],15)
		#self.caja("lt",self.transacciones[transaccion][4],[self.e_caja_grande[0]+self.e_caja_grande[2]*0.55,150+(y_secciones*40)],15,moneda=True)
		
		pygame.draw.rect(self.pantalla,recursos.BLACK,(self.e_caja_grande[0],self.e_caja_grande[1]+32,self.e_caja_grande[2],2))
		#y_secciones=0
		#mtext.render(self.pantalla,"Agregar transaccion",self.color_texto,[470,80])
		#self.boton_control_transaccion=mtext.render(self.pantalla,self.tipo_transaccion[self.control_tipo_transaccion],self.color_texto,[470,130])
		#chtext.render(self.pantalla,"Semana - "+str(date.today().isocalendar()[1]),self.color_texto,[1100,130])
		#self.dinero_monto=tb.update(self.pantalla,str(self.monto_transaccion),[800,130,200,30])
		#if self.tipo_transaccion[self.control_tipo_transaccion]=="Ingreso corriente":
	#		chtext.render(self.pantalla,"Sueldo",recursos.RED,[470,180])
		#	self.boton_control_cuenta=chtext.render(self.pantalla,self.nombres_cuentas[self.control_tipo_cuenta],recursos.GREEN,[800,180])
		#else:
		#	self.boton_control_cuenta=chtext.render(self.pantalla,self.nombres_cuentas[self.control_tipo_cuenta],recursos.RED,[470,180])

		#chtext.render(self.pantalla,"----------------->",recursos.WHITE,[600,180])

		#print self.monto_transaccion

		#if self.tipo_transaccion[self.control_tipo_transaccion]=="Gasto corriente":
		#	self.boton_control_gasto=chtext.render(self.pantalla,self.nombres_gastos[self.control_tipo_gasto],recursos.GREEN,[800,180])
		#if self.tipo_transaccion[self.control_tipo_transaccion]=="Aporte a deuda":
		#	self.boton_control_deuda=chtext.render(self.pantalla,self.nombres_deudas[self.control_tipo_deuda],recursos.GREEN,[800,180])

	def monetizar_cifra(self,num, simbolo="$",simbolo_2="MXN", n_decimales=2):
		n_decimales = abs(n_decimales)
		num = round(num, n_decimales)
		try:
			num, dec = str(num).split(".")
		except:
			num+=0.00
			num, dec = str(num).split(".")
		dec += "0" * (n_decimales - len(dec))
		num = num[::-1]
		l = [num[pos:pos+3][::-1] for pos in range(0,50,3) if (num[pos:pos+3])]
		l.reverse()
		num = str.join("'", l)
		try:
			if num[0:2] == "-,":
		   		 num = "-%s" % num[2:]
		except IndexError:
				pass
		return "%s %s.%s %s" % (simbolo, num, dec,simbolo_2)


	def redimencionando(self,tamano):
		while True:
			for event in pygame.event.get():
				if event.type==VIDEORESIZE:
					tamano=event.size
			if pygame.event.get()==[]:
				break
		return tamano
	def menu_principal(self):
		pygame.draw.rect(self.pantalla,self.color_secundario,(0,0,self.pantalla.get_width(),52))
		pygame.draw.rect(self.pantalla,self.color_secundario,(0,0,52,self.pantalla.get_height()))
		pygame.draw.rect(self.pantalla,self.color_personalizado,(0,52,self.pantalla.get_width(),3))
		pygame.draw.rect(self.pantalla,self.color_personalizado,(52,52,3,self.pantalla.get_height()))

		self.rect_nombre=self.caja("lt",self.nombre_propio,(60,0),len(self.nombre_propio),color_rect=None)
		a_boton=40
		self.caja("lt",self.balance,(self.a_nombre+a_boton+self.espacio_entre_indicadores,0),len(self.monetizar_cifra(self.balance)),color_rect=None,moneda=True)
		self.caja("lt",self.numero_entradas_diario,(self.a_nombre+self.a_balance+a_boton+(2*self.espacio_entre_indicadores),0),len(str(self.numero_entradas_diario)),color_rect=None,numero=True)
		self.caja("lt",self.numero_logros,(self.a_nombre+self.a_balance+self.a_entradas+a_boton+(3*self.espacio_entre_indicadores),0),len(str(self.numero_logros)),color_rect=None,numero=True)
		self.caja("lt",self.nivel,(self.a_nombre+self.a_balance+self.a_entradas+self.a_logros+a_boton+(4*self.espacio_entre_indicadores),0),len(str(self.nivel)),color_rect=None,numero=True)

		#print(self.rect_nombre)
		self.mostrar_botones(self.nombre_botones_menu_principal,self.botones_menu_principal,colision=2)
		self.mostrar_botones(self.nombre_botones_menu_principal_2,self.botones_menu_principal_2,colision=3)
	def mostrar_botones(self,nombres,botones,colision=0):
		for boton in nombres:
			if colision==0:
				if mouse.colliderect(botones[boton][0]):
					pygame.draw.rect(self.pantalla,self.color_principal,(botones[boton][2]-10,botones[boton][3]-5,52,42))
			else:
				if mouse.colliderect(botones[boton][0]):
					pygame.draw.rect(self.pantalla,self.color_principal,(botones[boton][2]-10,botones[boton][3]-5,52,42))
					if colision	==1:
						posicion=(botones[boton][2],botones[boton][3]-30)
					if colision	==2:
						posicion=(botones[boton][2]+52,botones[boton][3])
					if colision	==3:
						posicion=(botones[boton][2],botones[boton][3]+35)
					self.caja("cht",botones[boton][1],posicion,len(botones[boton][1]),color_rect=self.color_personalizado)
			botones[boton][0].update(self.pantalla,mouse,botones[boton][2],botones[boton][3])
			#print(boton,botones[boton][2],botones[boton][3])
	def main(self,):
		pygame.init()
		Corriendo=True
		paso=0
		while Corriendo==True:
			try:
				self.pantalla.fill(self.color_principal)
				if self.ventana=="Finanzas" or self.ventana=="Agregarfinanzas":
					self.finance()

				self.menu_principal()
				if paso<5: 
					self.pantalla_cargando()
				pygame.display.flip()
				mouse.update()
				for event in pygame.event.get():
					if event.type==pygame.KEYDOWN:
						pass
						#inkey = event.key
						#print(inkey)
						
					if event.type==QUIT: 
						pygame.display.quit()
						Corriendo=False
					elif event.type==VIDEORESIZE:
						tamano=self.redimencionando(event.size)
						if tamano[0]<1300 or tamano[1]<700:
							tamano=(1300,700)
						self.pantalla=pygame.display.set_mode(tamano,RESIZABLE)
						self.carga_dependencias()
						print("Redimencionado de la pantalla:  ",self.pantalla.get_width(),"-",self.pantalla.get_height())
						
					if event.type == pygame.MOUSEBUTTONDOWN:
						#print(mouse.left,mouse.top)
						#inkey = event.button
						#print(inkey)

						if self.ventana=="Finanzas":
							if mouse.colliderect(self.e_caja_verde):
								self.ventana="Agregarfinanzas"
						if self.ventana=="Agregarfinanzas":
							if mouse.colliderect(self.eleccion_semana) and event.button==4:
								self.semana_eleccion+=1
							if mouse.colliderect(self.eleccion_semana) and event.button==5:
								self.semana_eleccion-=1
							if self.semana_eleccion<1:
								self.semana_eleccion=1
							elif self.semana_eleccion>52:
								self.semana_eleccion=52

							if mouse.colliderect(self.eleccion_tipo_transaccion) and event.button==4:
								self.tipo_transaccion_eleccion+=1
							if mouse.colliderect(self.eleccion_tipo_transaccion) and event.button==5:
								self.tipo_transaccion_eleccion-=1
							if self.tipo_transaccion_eleccion<0:
								self.tipo_transaccion_eleccion=0
							elif self.tipo_transaccion_eleccion>2:
								self.tipo_transaccion_eleccion=2

							if self.tipo_transaccion_eleccion==0:
								if mouse.colliderect(self.eleccion_deuda) and event.button==4:
									self.deuda_eleccion+=1
								if mouse.colliderect(self.eleccion_deuda) and event.button==5:
									self.deuda_eleccion-=1
								if self.deuda_eleccion<0:
									self.deuda_eleccion=0
								elif self.deuda_eleccion>len(self.nombres_deudas)-1:
									self.deuda_eleccion=len(self.nombres_deudas)-1
							elif self.tipo_transaccion_eleccion==1:
								if mouse.colliderect(self.eleccion_gasto) and event.button==4:
									self.gasto_eleccion+=1
								if mouse.colliderect(self.eleccion_gasto) and event.button==5:
									self.gasto_eleccion-=1
								if self.gasto_eleccion<0:
									self.gasto_eleccion=0
								elif self.gasto_eleccion>9:
									self.gasto_eleccion=9

							if mouse.colliderect(self.comentario_transaccion):
								self.comentario_transaccion_mensaje=self.escribir_caja(self.comentario_transaccion_mensaje,self.comentario_transaccion,30,"alfanumerico","lt")
				paso+=1
							

			except Exception as e:
				print(e)
				self.pantalla.fill(self.color_principal)
				for event in pygame.event.get():
					if event.type==QUIT: 
						pygame.display.quit()
						Corriendo=False

if __name__ == '__main__':
	mouse=cursor()
	interfaz=interface()
	interfaz.main()