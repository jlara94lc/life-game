import pygame, string, threading
import recursos
from configparser  import SafeConfigParser
from datetime import date
from pygame.locals import *
from pygame import *

class cursor(pygame.Rect):
	def __init__(self): 
		pygame.Rect.__init__(self,0,0,1,1)
	def update(self):
		self.left,self.top=pygame.mouse.get_pos()
class text(object):
	def __init__(self): 
		pygame.font.init()
	def render(self,FontSize,text,pos,color_text=False,color_rect=False,moneda=False):
		if moneda!=False: text=interfaz.monetizar_cifra(text)
		if color_text==False: color_text=interfaz.color_texto
		if isinstance(text, int): text=str(text)
		FontName,rect_rect=interfaz.fuente_general,len(text)*FontSize/1.7
		self.font,self.size=pygame.font.Font(FontName,FontSize),FontSize
		for i in text.split("\r"):		
			if color_rect!=False: pygame.draw.rect(interfaz.pantalla,color_rect,[pos[0],pos[1],rect_rect,self.size])
			text=interfaz.pantalla.blit(self.font.render(i,1,color_text),pos)
			#pos[1] += self.size
		return text
	def edicion(self,FontSize,current_string,pos,tipo,numero_caracteres=False,moneda=False):
		if isinstance(current_string, int): current_string=str(current_string)
		if numero_caracteres==False: numero_caracteres=len(current_string)
		mayus=0
		while 1:
			if isinstance(current_string, int): current_string=str(current_string)
			pygame.draw.rect(interfaz.pantalla,Color('white'),[pos[0],pos[1],len(current_string)*FontSize/1.7,FontSize])
			event = pygame.event.poll()
			if event.type == KEYDOWN:
				inkey = event.key
				if inkey == 13:
					break
				elif inkey == K_BACKSPACE:
					current_string = current_string[0:-1]
				elif inkey == K_ESCAPE:
					break
			if len(current_string)<=numero_caracteres:
				if event.type == KEYUP:
					inkey = event.key
					if mayus==1 and inkey==301: mayus=0
				if event.type == KEYDOWN:
					inkey = event.key
					if mayus==0 and inkey==301: mayus=1
					if mayus==1: inkey-=32
					if tipo=="numerico" or tipo=="alfanumerico":
						if (inkey>=48 and inkey<=57) or (inkey>=256 and inkey<=265):
							if inkey>200:
								current_string+=chr(inkey-208)
							else:
								current_string+=chr(inkey)
					if tipo=="alfabeto" or tipo=="alfanumerico":
						if (inkey>=97 and inkey<=122) or (inkey>=65 and inkey<=90):
							current_string+=chr(inkey)
					if inkey==32:
						current_string+=" "
			if moneda==True: 
				if isinstance(current_string, str): current_string=int(current_string)
			self.render(FontSize,current_string,pos,color_text=Color('black'),color_rect=Color('white'),moneda=True)
			pygame.display.flip()
		if tipo=="numerico":
			return int(current_string)
		else:
			return current_string
class boton(object):
	def __init__(self, img1,img2,text):
		self.imgn=img1
		self.imgs=img2
		self.imga=self.imgn
		self.rect=self.imga.get_rect()
		self.texto=text
	def update(self,screen,cursor,h,l):
		self.rect.left,self.rect.top=(h,l)
		if cursor.colliderect(self.rect):
			screen.blit(self.imgs,self.rect)
			texto.render(20,self.texto,(h+self.rect[3],l+self.rect[3]/4),color_rect=interfaz.color_principal,color_text=interfaz.color_personalizado)
		else: 
			screen.blit(self.imgn,self.rect)

class scrollview(object):
	def __init__(self,caja,items,tamano_letra):
		#print(len(items))
		self.numero_items_scrollview=len(items)
		self.espacios_scrollview=int(caja[3]/tamano_letra)
		#print(self.espacios_scrollview)

		if self.numero_items_scrollview<self.espacios_scrollview: self.inicio_scrollview,self.final_scrollview=0,self.numero_items_scrollview
		else: self.inicio_scrollview,self.final_scrollview=0,self.espacios_scrollview
	def render(self):
		#print(self.inicio_scrollview,self.final_scrollview)
		for i in range(self.inicio_scrollview,self.final_scrollview,1):
			pass
			#print(i)
			#print(interfaz.nombres_cuentas[i])
		
class interface(object):
	def __init__(self):
		self.fuente_general="./Datos/fuentes/MontereyFLF-BoldItalic.ttf"
		self.pantalla=pygame.display.set_mode((1300,700),RESIZABLE)
		self.ventana,self.rect_nombre='Finanzas',[0,0,0,0]

		self.nombre_botones_menu_principal,self.nombre_botones_menu_principal_2,self.nombre_clases_gasto=[],[],[]
		self.botones_menu_principal={"Finanzas":[boton(recursos.finanzas,recursos.finanzas,'Finanzas'),"FINANZAS",10,60],"Diario":[boton(recursos.diario,recursos.diario,'Diario'),"DIARIO",10,102],"Logros":[boton(recursos.logros,recursos.logros,'Logros'),"LOGROS",10,144],"Configuracion":[boton(recursos.configuracion,recursos.configuracion,'Configuracion'),"CONFIGURACION",10,186],"Usuario":[boton(recursos.mando,recursos.mando,'Usuario'),"USUARIO",10,8]}
		self.botones_menu_principal_2={"Balance":[boton(recursos.finanzas,recursos.finanzas,'Balance'),"BALANCE",0,0],"Entradas":[boton(recursos.diario,recursos.diario,'Entradas'),"ENTRADAS",0,0],"Logros":[boton(recursos.logros,recursos.logros,'Logros'),"LOGROS",0,0],"Nivel":[boton(recursos.nivel,recursos.nivel,'Nivel'),"NIVEL",0,0],"Animo":[boton(recursos.smile,recursos.smile,'Animo'),"ANIMO",0,0]}
		self.clases_gasto={"Automovil":0,"Entretenimiento":0,"Comida":0,"Regalos":0,"Salud":0,"Casa":0,"Personal":0,"Mascotas":0,"Servicios":0,"Viajes":0,}
		self.cuentas,self.botones_cuentas,self.nombres_cuentas={},{},[]
		self.mensajecaja=123456

		self.nombre_propio,self.balance,self.numero_logros,self.numero_entradas_diario,self.nivel,self.deuda_total,self.fondo_mundial,self.monto_transaccion="",0,0,0,0,[0,[0,0,0,0]],0,0
		self.cargar_configuraciones()
		self.a_nombre=(self.rect_nombre[2]+60)
		self.espacio_entre_indicadores,self.a_balance,self.a_entradas,self.a_logros,self.a_nivel=0,400,100,100,100
		self.carga_dependencias()
	def cargar_configuraciones(self):
		for clave in self.botones_menu_principal.keys(): self.nombre_botones_menu_principal.append(clave)
		for clave in self.botones_menu_principal_2.keys(): self.nombre_botones_menu_principal_2.append(clave)
		for clave in self.clases_gasto.keys(): self.nombre_clases_gasto.append(clave)

		parser = SafeConfigParser()
		parser.read('./Datos/config.cfg')
		if parser.getint('general','color_')==0:
			self.color_principal=(30,30,30)
			self.color_secundario=(50,50,50)
			self.color_caja=(80,80,80)
			self.color_texto=(250,250,250)
		else:
			self.color_principal=(250,250,250)
			self.color_texto=(50,50,50)
		self.color_personalizado=Color(parser.get('general','color__'))
		self.imagenes_registradas=parser.getint('general','capturas')
		self.nombre_propio=parser.get('general','nombre')

		parser = SafeConfigParser()
		parser.read('./Datos/cuentas.cfg')
		self.numero_cuentas=len(parser.sections())
		for section in parser.sections():
			print(type(section))
			cadena=[]
			for valor,no in parser.items(section):
				cadena.append(int(no))
			self.cuentas[section]=cadena
			self.botones_cuentas[section]=0
		for clave in self.cuentas.keys(): self.nombres_cuentas.append(clave)

		#print(self.cuentas)

		self.cuentas_scrollview=scrollview([0,0,0,20],self.cuentas,10)

		"""parser = SafeConfigParser()
		parser.read('./Datos/transacciones.cfg')
		self.numero_transacciones=len(parser.sections())
		for section in parser.sections():
			cadena=[]
			for valor,no in parser.items(section):
				try:
					cadena.append(int(no))
				except:
					cadena.append(no)
			self.transacciones[int(section)]=cadena
		for transaccion in self.transacciones:
			anterior=0
			if self.transacciones[transaccion][0]=="D":
				self.cuentas[self.transacciones[transaccion][2]][0]+=self.transacciones[transaccion][4]
				self.cuentas[self.transacciones[transaccion][3]][0]+=self.transacciones[transaccion][4]
				self.gasto_acumulado+=self.transacciones[transaccion][4]
			if self.transacciones[transaccion][0]=="G":
				self.cuentas[self.transacciones[transaccion][2]][0]+=self.transacciones[transaccion][4]
				self.clases_gasto[self.transacciones[transaccion][3]]+=self.transacciones[transaccion][4]
				self.gasto_acumulado+=self.transacciones[transaccion][4]
			if self.transacciones[transaccion][0]=="S":
				self.cuentas[self.transacciones[transaccion][2]][0]+=self.transacciones[transaccion][4]
				self.ingreso_acumulado+=self.transacciones[transaccion][4]

		for cuenta in self.cuentas:
			self.cuentas[cuenta][2]=[0,0,0,0]
			if self.cuentas[cuenta][1]==1:
				self.balance+=self.cuentas[cuenta][0]
			if self.cuentas[cuenta][0]<0:
				self.deuda_total[0]+=self.cuentas[cuenta][0]

		self.nombres_cuentas,self.nombres_gastos,self.nombres_deudas=[],[],[]
		for key,value in self.cuentas.items():
			if value[0]!=0:
				if key=="Balance":
					self.nombres_cuentas.append(key)
				else:
					self.nombres_deudas.append(key)
		for key,value in self.clases_gasto.items():
			self.nombres_gastos.append(key)


		#print(self.cuentas)
		#print(self.nombres_cuentas)
		#print(self.nombres_deudas)
		print(self.transacciones)
		#print(len(self.transacciones))  """
	def carga_dependencias(self):
		self.a_nombre=(self.rect_nombre[2]+60)
		a_animo=52
		self.espacio_entre_indicadores=(self.pantalla.get_width()-self.a_nombre-self.a_balance-self.a_entradas-self.a_logros-self.a_nivel-a_animo)/5
		self.botones_menu_principal_2={"Balance":[boton(recursos.finanzas,recursos.finanzas,'Balance'),"BALANCE",self.a_nombre+self.espacio_entre_indicadores,8],"Entradas":[boton(recursos.diario,recursos.diario,'Entradas'),"ENTRADAS",self.a_nombre+self.a_balance+(2*self.espacio_entre_indicadores),8],"Logros":[boton(recursos.logros,recursos.logros,'Logros'),"LOGROS",self.a_nombre+self.a_balance+self.a_entradas+(3*self.espacio_entre_indicadores),8],"Nivel":[boton(recursos.nivel,recursos.nivel,'Nivel'),"NIVEL",self.a_nombre+self.a_balance+self.a_entradas+self.a_logros+(4*self.espacio_entre_indicadores),8],"Animo":[boton(recursos.smile,recursos.smile,'Animo'),"ANIMO",self.a_nombre+self.a_balance+self.a_entradas+self.a_logros+self.a_nivel+(5*self.espacio_entre_indicadores),8]}
	def pantalla_cargando(self,paso):
		if paso<5: 
			self.pantalla.fill(self.color_principal)
			texto.render(50,'Cargando',[10,10])
	def redimencionando(self,tamano):
		while True:
			for event in pygame.event.get():
				if event.type==VIDEORESIZE:
					tamano=event.size
			if pygame.event.get()==[]:
				break
		return tamano
	def monetizar_cifra(self,num, simbolo="$",simbolo_2="MXN", n_decimales=2):
		n_decimales = abs(n_decimales)
		num = round(num, n_decimales)
		try:
			num, dec = str(num).split(".")
		except:
			num+=0.00
			num, dec = str(num).split(".")
		dec += "0" * (n_decimales - len(dec))
		num = num[::-1]
		l = [num[pos:pos+3][::-1] for pos in range(0,50,3) if (num[pos:pos+3])]
		l.reverse()
		num = str.join("'", l)
		try:
			if num[0:2] == "-,":
		   		 num = "-%s" % num[2:]
		except IndexError:
				pass
		return "%s %s.%s %s" % (simbolo, num, dec,simbolo_2)
	def control_botones(self):
		while self.Corriendo:
			self.mostrar_botones(self.nombre_botones_menu_principal,self.botones_menu_principal)
	def mostrar_botones(self,nombres,botones):
		for boton in nombres:
			botones[boton][0].update(self.pantalla,mouse,botones[boton][2],botones[boton][3])

	def menu_principal(self):
		pygame.draw.rect(self.pantalla,self.color_secundario,(0,0,self.pantalla.get_width(),52))
		pygame.draw.rect(self.pantalla,self.color_secundario,(0,0,52,self.pantalla.get_height()))
		pygame.draw.rect(self.pantalla,self.color_personalizado,(0,52,self.pantalla.get_width(),3))
		pygame.draw.rect(self.pantalla,self.color_personalizado,(52,52,3,self.pantalla.get_height()))
		#s=15
		#if isinstance(s, int):
		#	print('its a string')
		#else:
		#	print('its another thing')
		#self.rect_nombre=self.caja("lt",self.nombre_propio,(60,0),len(self.nombre_propio),color_rect=None)
		a_boton=40
		#self.caja("lt",self.balance,(self.a_nombre+a_boton+self.espacio_entre_indicadores,0),len(self.monetizar_cifra(self.balance)),color_rect=None,moneda=True)
		#self.caja("lt",self.numero_entradas_diario,(self.a_nombre+self.a_balance+a_boton+(2*self.espacio_entre_indicadores),0),len(str(self.numero_entradas_diario)),color_rect=None,numero=True)
		#self.caja("lt",self.numero_logros,(self.a_nombre+self.a_balance+self.a_entradas+a_boton+(3*self.espacio_entre_indicadores),0),len(str(self.numero_logros)),color_rect=None,numero=True)
		#self.caja("lt",self.nivel,(self.a_nombre+self.a_balance+self.a_entradas+self.a_logros+a_boton+(4*self.espacio_entre_indicadores),0),len(str(self.nivel)),color_rect=None,numero=True)

		self.mostrar_botones(self.nombre_botones_menu_principal,self.botones_menu_principal)
		self.mostrar_botones(self.nombre_botones_menu_principal_2,self.botones_menu_principal_2)
	def finance(self):
		self.separacion=20
		self.e_caja_pequena=self.RectangulosRedondeados((55+self.separacion,120,(0.3)*(self.pantalla.get_width()-(55+(3*self.separacion))),self.pantalla.get_height()-(120+self.separacion)),self.color_caja,0.05)
		self.e_caja_grande=self.RectangulosRedondeados((self.e_caja_pequena[0]+self.e_caja_pequena[2]+self.separacion,120,(0.7)*(self.pantalla.get_width()-(55+(3*self.separacion))),self.e_caja_pequena[3]),self.color_caja,0.05)
		self.e_caja_verde=self.RectangulosRedondeados((self.e_caja_grande[0],self.pantalla.get_height()-(50+self.separacion),self.e_caja_grande[2]/2,50),Color("green"),0.05)
		self.e_caja_roja=self.RectangulosRedondeados((self.e_caja_grande[0]+self.e_caja_grande[2]/2,self.pantalla.get_height()-(50+self.separacion),self.e_caja_grande[2]/2,50),Color("red"),0.05)
		self.mostrar_cuentas()
		self.cuentas_scrollview.render()
		#if self.ventana=="Finanzas":
		#	self.mostrar_transacciones()
		#else:
		#	self.agregar_transaccion_pantalla()
	def mostrar_cuentas(self):
		y_secciones=0
		texto.render(30,"Cuentas",[55+self.separacion+20,70])
		#self.caja("lt","Cuentas",[55+self.separacion+20,70],7,color_rect=None)
		
		#elf.caja("cht","Cuenta",[55+self.separacion+10,130],15,color_rect=None)
		#self.caja("cht","Monto",[(55+self.separacion+self.e_caja_pequena[2]-70),130],15,color_rect=None)
		pygame.draw.rect(self.pantalla,recursos.BLACK,(self.e_caja_pequena[0],self.e_caja_pequena[1]+32,self.e_caja_pequena[2],2))

		#self.caja("cht","Balance",[55+self.separacion+10,160],15,color_rect=None)
		#self.cuentas["Balance"][2]=self.caja("cht",self.cuentas["Balance"][0],[(55+self.separacion+self.e_caja_pequena[2])-self.cuentas["Balance"][2][2]-5,160],20,color_rect=None,moneda=True)
		#for cuenta in self.cuentas:
		#	if self.cuentas[cuenta][0]!=0 and cuenta!="Balance":
		#		self.caja("cht",cuenta,[55+self.separacion+10,190+(y_secciones*30)],15,color_rect=None)
		#		self.cuentas[cuenta][2]=self.caja("cht",self.cuentas[cuenta][0],[(55+self.separacion+self.e_caja_pequena[2])-self.cuentas[cuenta][2][2]-5,190+(y_secciones*30)],20,color_rect=None,moneda=True)
		#		y_secciones+=1


		#pygame.draw.rect(self.pantalla,recursos.BLACK,(self.e_caja_pequena[0],self.pantalla.get_height()-(30+self.separacion),self.e_caja_pequena[2],2))
		#self.caja("cht","Deuda total",[55+self.separacion+10,self.pantalla.get_height()-(25+self.separacion)],15,color_rect=None)
		#chtext.render(self.pantalla,"Deuda total",self.color_texto,[110,self.pantalla.get_height()-(120+self.separacion)])
		#print(self.deuda_total)
		#self.deuda_total[1]=self.caja("cht",self.deuda_total[0],[(55+self.separacion+self.e_caja_pequena[2])-self.deuda_total[1][2]-5,self.pantalla.get_height()-(25+self.separacion)],20,color_rect=None,moneda=True)
		#chtext.render(self.pantalla,str(self.deuda_total),recursos.RED,[350,650])

	def scrollview(self,caja,items,tamano_letra):
		for clave in self.cuentas.keys(): self.nombres_cuentas.append(clave)
		print(len(items))
		self.numero_items_scrollview=len(items)
		self.espacios_scrollview=int(caja[3]/tamano_letra)
		print(self.espacios_scrollview)

		if self.numero_items_scrollview<self.espacios_scrollview: self.inicio_scrollview,self.final_scrollview=0,self.numero_items_scrollview-1
		else: self.inicio_scrollview,self.final_scrollview=0,self.espacios_scrollview-1
		
		print(self.inicio_scrollview,self.final_scrollview)
		for i in range(self.inicio_scrollview,self.final_scrollview-1,1):
			print(self.nombres_cuentas[i])

	def RectangulosRedondeados(self,rect,color,radius=0.4):
		rect         = Rect(rect)
		color        = Color(*color)
		alpha        = color.a
		color.a      = 0
		pos          = rect.topleft
		rect.topleft = 0,0
		rectangle    = Surface(rect.size,SRCALPHA)
		circle       = Surface([min(rect.size)*3]*2,SRCALPHA)
		draw.ellipse(circle,(0,0,0),circle.get_rect(),0)
		circle       = transform.smoothscale(circle,[int(min(rect.size)*radius)]*2)
		radius              = rectangle.blit(circle,(0,0))
		radius.bottomright  = rect.bottomright
		rectangle.blit(circle,radius)
		radius.topright     = rect.topright
		rectangle.blit(circle,radius)
		radius.bottomleft   = rect.bottomleft
		rectangle.blit(circle,radius)
		rectangle.fill((0,0,0),rect.inflate(-radius.w,0))
		rectangle.fill((0,0,0),rect.inflate(0,-radius.h))
		rectangle.fill(color,special_flags=BLEND_RGBA_MAX)
		rectangle.fill((255,255,255,alpha),special_flags=BLEND_RGBA_MIN)
		return self.pantalla.blit(rectangle,pos)
	def main(self,):
		pygame.init()
		self.Corriendo=True
		self.actualizando,control_clicks=0,0
		self.errores=0

		#x = threading.Thread(target=self.control_botones)
		#x.start()
		while self.Corriendo:
			try:
				self.pantalla.fill(self.color_principal)
				if self.ventana=="Finanzas" or self.ventana=="Agregarfinanzas":
					self.finance()

				self.menu_principal()
				self.pantalla_cargando(self.actualizando)
				pygame.display.flip()
				mouse.update()

				for event in pygame.event.get():
					if event.type==pygame.KEYDOWN:
						pass
						
					if event.type==QUIT: 
						pygame.display.quit()
						self.Corriendo=False
					if event.type==VIDEORESIZE:
						tamano=self.redimencionando(event.size)
						if tamano[0]<1300 or tamano[1]<700:
							tamano=(1300,700)
						self.pantalla=pygame.display.set_mode(tamano,RESIZABLE)
						self.carga_dependencias()
						print("Redimencionado de la pantalla:  ",self.pantalla.get_width(),"-",self.pantalla.get_height())
						
					if control_clicks==1:
						if event.type==pygame.MOUSEBUTTONDOWN:
							control_clicks=1
						else:
							control_clicks=0	
					if event.type == pygame.MOUSEBUTTONDOWN:
						if control_clicks==0:

							if event.type == pygame.MOUSEBUTTONDOWN:
								click_mouse = event.button
								#4=arrina
								#5=abajo
								print('controles',self.cuentas_scrollview.numero_items_scrollview,self.cuentas_scrollview.espacios_scrollview)
								
								if self.cuentas_scrollview.numero_items_scrollview>self.cuentas_scrollview.espacios_scrollview: 
									if self.cuentas_scrollview.inicio_scrollview>0:
										if click_mouse==5:
											print('abajo')
											print('controles_2',self.cuentas_scrollview.inicio_scrollview,self.cuentas_scrollview.final_scrollview)
											self.cuentas_scrollview.inicio_scrollview-=1
											self.cuentas_scrollview.final_scrollview-=1
											print('controles_2',self.cuentas_scrollview.inicio_scrollview,self.cuentas_scrollview.final_scrollview)
									if self.cuentas_scrollview.final_scrollview<self.cuentas_scrollview.numero_items_scrollview:
										if click_mouse==4:
											print('arriba')
											print('controles_2',self.cuentas_scrollview.inicio_scrollview,self.cuentas_scrollview.final_scrollview)
											self.cuentas_scrollview.inicio_scrollview+=1
											self.cuentas_scrollview.final_scrollview+=1
											print('controles_2',self.cuentas_scrollview.inicio_scrollview,self.cuentas_scrollview.final_scrollview)

									if self.cuentas_scrollview.inicio_scrollview<0: self.cuentas_scrollview.inicio_scrollview=0
									if self.cuentas_scrollview.final_scrollview>self.cuentas_scrollview.numero_items_scrollview-1: self.cuentas_scrollview.final_scrollview=self.cuentas_scrollview.numero_items_scrollview-1


								print(click_mouse)

							if event.type==pygame.MOUSEBUTTONDOWN:
								control_clicks=1
				self.actualizando+=1

			except Exception as e:
				print(e)
				self.errores+=1
				if self.errores==10:
					print('Demasiados errores')
					pygame.display.quit()
					self.Corriendo=False
				for event in pygame.event.get():
					if event.type==QUIT: 
						pygame.display.quit()
						self.Corriendo=False

if __name__ == '__main__':
	mouse=cursor()
	interfaz=interface()
	texto=text()
	interfaz.main()